

#define heap_header_size (sizeof(unsigned int) + sizeof(unsigned int))

typedef struct heap
{
    unsigned int capacity;
    unsigned int size;
    int data[0];
} min_heap;



min_heap * create_min_heap(unsigned int capacity);

void hp_insert(int value, min_heap * heap);
int hp_pop(min_heap * heap);

int hp_parent(int i);
int hp_left(int i);
int hp_right(int i);

void hp_bubble_down(int k, int n, min_heap * heap);
void hp_bubble_up(int n, min_heap * heap);

void hp_swap(int a, int b, min_heap * heap);

