#include "heap.h"

#include <stdlib.h>
#include <stdio.h>



min_heap * create_min_heap(unsigned int capacity)
{
    min_heap * heap = (min_heap *)malloc(
        heap_header_size + (capacity * sizeof(int))
    );

    heap->capacity = capacity;
    heap->size = 0;
}


void hp_insert(int value, min_heap * heap)
{
    if (heap->size == heap->capacity)
    {
        printf("The heap says you had your chance (over capacity)\n");
        int * wait_for_it = NULL;
        int segfault = *wait_for_it;
        //"error handling"
    }

    heap->data[heap->size++] = value;
    hp_bubble_up(heap->size - 1, heap);
}


int hp_pop(min_heap * heap)
{
    if (heap->size == 0)
    {
        printf("The heap says you had your chance (index < 0)\n");
        int * wait_for_it = NULL;
        int segfault = *wait_for_it;
    }

    int value = heap->data[0];

    hp_swap(0, heap->size - 1, heap);
    heap->size--;
    hp_bubble_down(0, heap->size - 1, heap);

    return value;
}


int hp_parent(int i)
{
    return (i - 1) / 2;
}


int hp_left(int i)
{
    return i * 2 + 1;
}


int hp_right(int i)
{
    return i * 2 + 2;
}


void hp_bubble_down(int k, int n, min_heap * heap)
{
    while (2 * k + 1 <= n)
    {
        int l = hp_left(k);
        int r = hp_right(k);
        if (r <= n)
            l = (heap->data[l] < heap->data[r]) ? l : r;

        if (heap->data[k] < heap->data[l])
            break;

        hp_swap(k, l, heap);
        k = l;
    }
}


void hp_bubble_up(int n, min_heap * heap)
{
    while (n > 0 && heap->data[n] < heap->data[hp_parent(n)])
    {
        hp_swap(n, hp_parent(n), heap);
        n = hp_parent(n);
    }
}


void hp_swap(int a, int b, min_heap * heap)
{
    int temp = heap->data[b];
    heap->data[b] = heap->data[a];
    heap->data[a] = temp;
}


