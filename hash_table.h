#include "types.h"
#include "linked_list.h"

#define ht_header_size sizeof(unsigned int)



/*
 *  Hash Table for 1 Byte Keys
 *
 *  Collisions are handled with chaining.
 *
 *  The Key size may be redefined using the ht_key_t definition
 *  Be aware that rotl8() and rotr8() will need to be re-implemented
 *  if the key size changes.
 *
 *  It is important to note that the hash_table struct may never be passed
 *  to a function directly. A pointer must always be used.
 */

typedef struct hash_table
{
    unsigned int table_size;    //number of entries in hash table
    linked_list * table[0];     //must be changed to entry_t table[0];
} hash_table;




/****************************
 *                          *
 *   Interface Functions    *
 *                          *
 ****************************/

/*
 *  Create a New Hash Table
 *
 *  table_size: Number of buckets in the hash table
 *  chain_len:  Maximum number of links in the chain
 *              0: unbounded
 *
 *  Example:
 *      hash_table * table = create_hash_table(128, 0);
 *
 *  Creates a hash table with 128 bucket, all of which may grow
 *  indefinitely.
 */
hash_table * create_hash_table(unsigned int table_size,
                               unsigned int chain_len);

/*
 *  Returns the value of the K:V pair
 */
ht_value_t ht_get(ht_key_t key, hash_table *table);


/*
 *  Insert a new K:V pair into the hash table
 */
void ht_insert(ht_key_t key, ht_value_t value, hash_table * table);


/*
 *  Deletes a K:V pair from the hash table, by key
 */
void ht_delete(ht_key_t key, hash_table * table);




/****************************
 *                          *
 *   Internal Functions     *
 *                          *
 ****************************/

/*
 *  Return the chain mapped to by the hash function for a given key
 */
linked_list * ht_get_chain(ht_key_t key, hash_table * table);


/*
 *  Returns the hash code for a given key
 */
int n_hash(ht_key_t key);


/*
 *  Maps the hash code to a table entry
 */
ht_key_t compress(int hash, hash_table * table);


/*
 *  8-bit Circular Shift Left
 */
ht_key_t rotl8(ht_key_t byte, unsigned int shift);


/*
 *  8-bit Circular Shift Right
 */
ht_key_t rotr8(ht_key_t byte, unsigned int shift);




/****************************
 *                          *
 *      Print Functions     *
 *                          *
 ****************************/

/*
 *  Print the hash table
 */
void ht_print_table(hash_table * table);

