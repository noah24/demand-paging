#!/bin/bash

cfiles=`ls | grep -E "\.c$"`
gcc -g -c $cfiles

ofiles=`ls | grep -E "\.o$"`
gcc -g $ofiles -o project330
status=$?

rm *.o

if [ $status -eq 0 ]
then
    ./project330
fi
