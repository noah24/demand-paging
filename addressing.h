#include "types.h"

#define addr_offset_mask 0xff
#define addr_page_mask 0xff00
#define addr_offset_width 8
#define addr_frame_size 0x100

/*
 *  Anatomy of a Logical Address
 *
 *  ====================================
 *  |                |        |        |
 *  |     unused     |  page  | offset |
 *  |                |        |        |
 *  ====================================
 *   31            16 15     8 7      0
 *
 *
 *  Offset width: 8
 *  Page   width: 8
 *  Unused width: 16
 */


/*
 *  Returns the offset portion of a logical address
 *  Example:
 *      addr_offset_t offset = get_offset(address);
 */
addr_offset_t get_offset(addr_t address);


/*
 *  Returns the page number portion of a logical address
 *  Example:
 *      p_number_t page_number = get_page_number(address);
 */
p_number_t get_page_number(addr_t address);


/*
 *  Takes a frame number and offset and returns the physical address
 *  The frame number should be retreived from the page table
 *  Example:
 *      p_number_t frame_number = get_frame_number(table_entry);
 *      addr_offset_t offset = get_offset(addr);
 *      addr_t physical_addr = gen_phys_addr(frame_number, offset);
 *      char data = main_mem[physical_addr];
 */
addr_t gen_phys_addr(p_number_t frame_number, addr_offset_t offset);


