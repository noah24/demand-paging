#include "addressing.h"



/*
 *  Anatomy of a Logical Address
 *
 *  ====================================
 *  |                |        |        |
 *  |     unused     |  page  | offset |
 *  |                |        |        |
 *  ====================================
 *   31            16 15     8 7      0
 *
 *
 *  Offset width: 8
 *  Page   width: 8
 *  Unused width: 16
 */

addr_offset_t get_offset(addr_t address)
{
    return address & addr_offset_mask;
}


p_number_t get_page_number(addr_t address)
{
    return (address & addr_page_mask) >> addr_offset_width;
}


addr_t gen_phys_addr(p_number_t frame_number, addr_offset_t offset)
{
    return (frame_number << addr_offset_width) | offset;
}
