#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"


linked_list * create_linked_list(unsigned int max_length)
{
    linked_list * list = (linked_list *)malloc(sizeof(linked_list));

    list->head = NULL;
    list->tail = NULL;

    list->node_count = 0;
    list->max_length = max_length;

    return list;
}


list_node * create_node(p_number_t key, ll_value_t value)
{
    list_node *node = (list_node*)malloc(sizeof(list_node));
    node->key = key;
    node->value = value;
    node->next = NULL;
    node->previous = NULL;

    return node;
}


list_node * _ll_search(p_number_t key, linked_list * list)
{
    list_node * head = list->head;

    while (head != NULL)
    {
        if (head->key == key)
            return head;
        head = head->next;
    }

    return NULL;
}


void LRU_insert(p_number_t key, ll_value_t value, linked_list * list)
{
    list_node * node = _ll_search(key, list);
    if (node == NULL)
    {
        ll_insert(key, value, list);
    }
    else if (node->value != value)
    {
         ll_delete_node(&node, list);
         ll_insert(key, value, list);
    }
    else
    {
        ll_escalate_node(node, list);
    }
}


void ll_insert(p_number_t key, ll_value_t value, linked_list * list)
{
    list_node * node = create_node(key, value);
    insert_node_head(node, list);
}


void insert_node_tail(list_node * target, linked_list * list)
{
    ll_trim_if_full(list);

    if (list->tail == NULL)
    {
        list->head = target;
        list->tail = target;
    }
    else
    {
        target->previous = list->tail;
        (list->tail)->next = target;
        list->tail = target;
    }

    list->node_count++;
}


void insert_node_head(list_node * target, linked_list * list)
{
    ll_trim_if_full(list);

    if (list->head == NULL)
    {
        list->head = target;
        list->tail = target;
    }
    else
    {
        target->next = list->head;
        (list->head)->previous = target;
        list->head = target;
    }

    list->node_count++;
}


void ll_delete_front(linked_list * list)
{
    if (list->head == list->tail)
    {
        free(list->head);
        list->head = NULL;
        list->tail = NULL;
    }
    else
    {
        list->head = (list->head)->next;
        free((list->head)->previous);
        (list->head)->previous = NULL;
    }

    list->node_count--;
}


void ll_delete_back(linked_list * list)
{
    if (list->head == list->tail)
    {
        free(list->head);
        list->head = NULL;
        list->tail = NULL;
    }
    else
    {
        list->tail = (list->tail)->previous;
        free((list->tail)->next);
        (list->tail)->next = NULL;
    }

    list->node_count--;
}


void ll_delete_node(list_node ** target, linked_list * list)
{
    if (list->head == list->tail)
    {
        list->head = NULL;
        list->tail = NULL;
    }
    else if (list->head == *target)
    {
        list->head = (*target)->next;
        (list->head)->previous = NULL;
    }
    else if (list->tail == *target)
    {
        list->tail = (*target)->previous;
        (list->tail)->next = NULL;
    }
    else
    {
        ((*target)->previous)->next = (*target)->next;
        ((*target)->next)->previous = (*target)->previous;
    }

    free(*target);
    *target = NULL;

    list->node_count--;
}


void ll_escalate_node(list_node * target, linked_list * list)
{
    if (list->head == list->tail || list->head == target)
    {
        return;
    }
    else if (list->tail == target)
    {
        list->tail = target->previous;
        (list->tail)->next = NULL;
    }
    else
    {
        (target->previous)->next = target->next;
        (target->next)->previous = target->previous;
    }

    target->previous = NULL;
    target->next = list->head;
    (list->head)->previous = target;
    list->head = target;
}


void print_linked_list(linked_list * list)
{
    list_node * head = list->head;
    list_node * tail = list->tail;

    if (head == NULL)
        printf("The list is empty\n\n");

    else
    {
        printf("HEAD-->");
        while (head != NULL)
        {
            printf("<--{%d, %d}-->",
                head->key, head->value);
            head = head->next;
        }
        printf("<--TAIL\n");
    }
}


void print_linked_list_experimental(linked_list * list)
{
    list_node * head = list->head;
    list_node * tail = list->tail;

    if (head == NULL)
        printf("The list is empty\n\n");

    else
    {
        printf("Forwards:\nHEAD-->");
        while (head != NULL)
        {
            printf("<--{%d, %d}-->",
                head->key, head->value);
            head = head->next;
        }
        printf("<--TAIL\n\nBackwards:\nTAIL-->");
        while (tail != NULL)
        {
            printf("<--{%d, %d}-->",
                tail->key, tail->value);
            tail = tail->previous;
        }
        printf("<--HEAD\n\n");
    }
}



ll_value_t get_value(list_node *target)
{
    return target->value;
}


ll_key_t get_key(list_node *target)
{
    return target->key;
}


ll_key_t ll_get_LRU_key (linked_list * list)
{
    return list->tail->key;
}


ll_value_t ll_get_LRU_value (linked_list * list)
{
    return list->tail->value;
}


void set_value(list_node *target, ll_value_t value)
{
    target->value = value;
}


void set_key(list_node *target, p_number_t key)
{
    target->key = key;
}


void ll_trim_if_full(linked_list * list)
{
    if (list->max_length > 0 && list->node_count == list->max_length)
        ll_delete_back(list);
}


ll_value_t ll_search(p_number_t key, linked_list *list)
{
    list_node * target = _ll_search(key, list);

    if (target != NULL)
        return get_value(target);

    return ll_key_not_found;
}


void ll_delete(p_number_t key, linked_list * list)
{
    list_node * target = _ll_search(key, list);
    if (target != NULL)
        ll_delete_node(&target, list);
}
