#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "types.h"

#define ll_key_not_found 0


/*
 *  PREAMBLE
 *
 *  This list is specifically designed for page and frame number keys
 *  with page table entries as values.
 *
 *  Despite this, it may be easily adapted to use any Key: Value pairs.
 */




/****************************
 *                          *
 *        Structures        *
 *                          *
 ****************************/

/*
 *  Anatomy of a list_node
 *
 *               ===========================
 *               |        |                |
 *       <------ |  Key:  |     :Value     | ------>
 *         prev  |        |                |  next
 *               ===========================
 *         ptr     8 bits       16 bits        ptr
 *
 *
 *
 */

typedef struct list_node
{
  ll_key_t key;                     //Key is recommended to be unique
  ll_value_t value;                 //Value of the K: V pair
  struct list_node *next;           //Points to the next node or NULL
  struct list_node *previous;       //Points to the previous node or NULL
} list_node;


typedef struct linked_list
{
    list_node * head;               //Pointer to the tail of the linked list
    list_node * tail;               //Pointer to the head of the linked list
    unsigned int max_length;        //0 for unbounded list length
    unsigned int node_count;        //Current number of nodes in the list
} linked_list;




/****************************
 *                          *
 *   Interface Functions    *
 *                          *
 ****************************/

/*
 *  Create and initialize a doubly-linked list
 *
 *  Returns a pointer to the linked_list struct
 *
 *  Example:
 *      linked_list * list = create_linked_list();
 */
linked_list * create_linked_list(unsigned int max_length);


/*
 *  Insert a node according to the Least Recently Used principle.
 *  If the K:V exists in the list, it will be brought to the head
 *  Otherwise, the K:V will be inserted at the head. If the list is at
 *  maximum length, then it will drop the tail node.
 *
 *  If a node exists with a Key K, but a node->value != value (arg), then
 *  the existing node will be removed, and a new node will be LRU inserted.
 */
void LRU_insert(p_number_t key, ll_value_t value, linked_list * list);


/*
 *  Search the list for a given key and return the corresponding value
 *
 *  Returns a valid ll_value_t if the key is found
 *  and ll_key_not_found otherwise
 */
ll_value_t ll_search(p_number_t key, linked_list *list);


/*
 *  Insert a K: V pair at the head of the linked list
 */
void ll_insert(p_number_t key, ll_value_t value, linked_list * list);


/*
 *  Delete a node in the list by its key. If multiple nodes exist in the
 *  list with the same key, it will delete the one closest to the head.
 */
void ll_delete(p_number_t key, linked_list * list);


/*
 *  Delete the list_node at the front of the list
 */
void ll_delete_front(linked_list * list);


/*
 *  Delete the list_node at the back of the list
 */
void ll_delete_back(linked_list * list);


/*
 *  Return the value from the node at the back of the list
 */
ll_value_t ll_get_LRU_value (linked_list * list);


/*
 *  Return the valkeyue from the node at the back of the list
 */
ll_key_t ll_get_LRU_key (linked_list * list);




/****************************
 *                          *
 *   Internal Functions     *
 *                          *
 ****************************/

/*
 *  Creates a new list_node
 *  Takes a key and value, and returns a pointer to the list_node.
 *  Example:
 *      list_node * node = create_node(1, 10);
 */
list_node * create_node(p_number_t key, ll_value_t value);



/*
 *  Returns a list_node's value
 */
ll_value_t get_value(list_node *target);



/*
 *  Returns a list_node's key
 */
p_number_t get_key(list_node *target);



/*
 *  Set the list_node's value
 *
 *  Pass it a pointer to a list_node, and the desired value
 *  Example:
 *      set_value(target);
 */
void set_value(list_node *target, ll_value_t value);



/*
 *  Set the list_node's key
 *
 *  Pass it a pointer to a list_node, and the desired key
 *  Example:
 *      set_key(target, key);
 */
void set_key(list_node *target, p_number_t key);



/*
 *  Search the list for a list_node by its key
 *  Returns a pointer to the node if found, NULL otherwise
 *  Example:
 *      list_node * node = _ll_search(10, list);
 */
list_node * _ll_search(p_number_t key, linked_list * list);



/*
 *  Add a new list_node to the front of the list
 *  Do NOT try to insert existing nodes in the list
 */
void insert_node_head(list_node * target, linked_list * list);



/*
 *  Add a new list_node to the back of the list
 *  Do NOT try to insert existing nodes in the list
 */
void insert_node_tail(list_node * target, linked_list * list);



/*
 *  Move a list_node to the front of the list
 */
void ll_escalate_node(list_node * target, linked_list * list);



/*
 *  Delete a list_node located in the list
 *  The passed node pointer will be set NULL
 *  Example:
 *      ll_delete_node(&node4, list);
 */
void ll_delete_node(list_node ** target, linked_list * list);



/*
 *  Remove the last node in the list iff the list is at max length
 *  Example:
 *      ll_trim_if_full(list);
 */
void ll_trim_if_full(linked_list *list);




/****************************
 *                          *
 *      Print Functions     *
 *                          *
 ****************************/

/*
 *  Print the key:value of each node in the list
 */
void print_linked_list(linked_list * list);


/*
 *  Print the page-key & frame-value of each node in the list
 *  first from head to tail, and then from tail to head
 */
void print_linked_list_experimental(linked_list * list);


#endif
