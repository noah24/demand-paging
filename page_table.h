#ifndef PAGE_TABLE_H
#define PAGE_TABLE_H


#include "types.h"
#include "linked_list.h"


#define p_num_entries 0xFF
#define p_frame_num_flag 0x007F
#define p_inverted_frame_field_mask 0xff80
#define p_valid_flag 0x8000
#define p_invalid_flag 0x7FFF
#define p_VALID 1
#define p_INVALID 0
#define p_table_size 256
#define p_max_entries 128
#define p_page_field_mask 0x7F80
#define p_frame_field_width 7

/*
 *  PREAMBLE
 *
 *  This file offers you a variety of functions for interacting with page
 *  table entries.
 *
 *  Stucture of the Page Table
 *  The page table is an array of 16 bit unsigned integers with 255 entries.
 *  It can be initialized in main with:
 *      page_table * table = init_page_table();
 *
 *  The page table associates page numbers to frame numbers.
 *     Page number: represented by the index of the page table
 *    Frame number: contained in the page table entry
 *
 *  The page number and frame number are both 8 bits. The page table entries
 *  are 16 bits so they may carry additional information, such as the V/I bit.
 *
 *  Do not directly set the Valid/Invalid (V/I) bit. Doing so will be a source
 *  of error and pain. There are two functions set_valid and set_invalid which
 *  can be used to set the V/I bit.
 *
 *  Similarly, do not directly set frame numbers within a page table entry.
 *
 *  Anatomy of a Page Table Entry
 *
 *  ====================
 *  |V|        |       |
 *  |/|  pnum  |  f_n  |
 *  |I|        |       |
 *  ====================
 *   15       7 6     0
 *
 *
 *  Bits 7 to 14 contain the page number in the case that pt_search() releases
 *  the LRU frame
 *
 *  There are pre-defined constants p_VALID and p_INVALID for the V/I bit.
 *  Use them.
 */



 /*
  *  The page_table struct contains 3 parts
  *  frame_count: an unsigned int representing the current number
  *               of valid entries (maximum 128)
  *  p_table: a table of 16 bit unsigned ints representing page entries
  *           as described above
  *  p_table_queue: A doubly linked list that tracks the most and least
  *                 recently used entries (head and tail respectively)
  *NOTES
  * The table has 256 entries to correspond with 256 possible page numbers.
  * Only 128 entries can be "valid" at any given time, however, reflecting
  * the fact that we only have 128 frames in physical memory.
  * As such, only 128 frames can be valid at any given time. After 128
  * frames have been filled (switched to valid) a frame must be removed
  * (made invalid) before a new one can be entered. When the page table has
  * not yet reached 128 entries we simply add frame values in ascending
  * order, begining with 0 and ending with 127. After the page table
  * is full the p_table_queue determines what will occur next.
  * The table_queue tracks the least recently used entry and offers this entry
  * up for replacement.
  *
  */
typedef struct page_table
{
  unsigned int frame_count;
  p_entry_t p_table[p_table_size];
  linked_list * p_table_queue;
}page_table;


/*
 *  Creates a new Page Table and returns a pointer to that Page Table
 *
 *  Example:
 *
 *      page_table * p_table = init_page_table();
 */
page_table * init_page_table ();


/*
 *  Searches the page table by page number and returns the corresponding
 *  table entry when the entry is valid or an entry that contains the next
 *  available frame when the entry is invalid.
 *
 *  NOTE:
 *    When the entry is valid this operation also updates the entry
 *  indicating that is was the most recently used.
 *    When the entry is invalid and the there are open frames the entry
 *  contains the frame corresponding with the next available frame.
 *    When the entry is invalid and there are no frames available the
 *  entry contains the frame corresponding with the LRU entry.
 *
 *  Pass it a pointer to a Page Table and a page number
 *
 *  Complexity: O(n) when entry is valid, O(1) otherwise
 *
 *  Example:
 *
 *    p_entry_t table_entry = pt_search(table, page_number);
 */
 p_entry_t  pt_search(page_table * table, p_number_t page_number);


 /*
  *  Inserts an entry into the page table.
  *
  *  Pass it a pointer to a Page Table, a page number, and a frame number.
  *
  *  Complexity: O(1)
  *
  *  Example:
  *
  *    pt_insert(table, page_number, frame_number);
  */
 void pt_insert(page_table * table, p_number_t page_number, p_number_t frame);


/*
 *  re-doc
 */
p_entry_t get_LRU_entry(page_table * table);


/*
 *  Returns 1 (TRUE) if the page entry is valid, and 0 (FALSE) otherwise
 *
 *  Pass it a the value of a table entry
 *  Example:
 *
 *    if(pt_entry_valid(table_entry)) {   * retrieve data *   }
 */
 int pt_entry_valid (p_entry_t table_entry);

/*
 *  Returns 1 (TRUE) if the page table is full (128 valid entries)
 *  or 0 (FALSE) otherwise
 *
 *  Pass it a pointer to a page table
 *  Example:
 *
 *    if (pt_full(table)) {  * take action *  }
 */
int pt_full (page_table * table);


/*
 *  Set the V/I bit of a page table entry as Valid
 *
 *  Pass it the address of a page table entry
 *  Example:
 *
 *      set_valid( &(page_table[1]) );
 */
void set_valid(p_entry_t * table_entry);


/*
 *  Set the V/I bit of a page table entry as Invalid
 *
 *  Pass it the address of a page table entry
 *  Example:
 *
 *      set_invalid( &(page_table[1]) );
 */
void set_invalid(p_entry_t * table_entry);


/*
 *  Get the V/I bit of a page table entry
 *  Returns either 0 or 1
 *
 *  Pass it the page table entry
 *  Example:
 *      get_vi_bit( page_table[1] );
 *      printf("vi bit: %x\n", get_vi_bit(page_table[1]));
 */
p_entry_t get_vi_bit(p_entry_t table_entry);


/*
 *  Get the frame number portion of a page table entry
 *
 *  Example:
 *      get_frame_number( page_table[1] );
 *      printf("frame_number: 0x%x\n", get_frame_number(page_table[1]));
 */
p_number_t get_frame_number(p_entry_t table_entry);


/*
 *  Set the frame number portion of a page table entry
 *
 *  Takes the address of a page table entry and the value for the frame number
 *  Example:
 *      set_frame_number(&page_table[1], 0x1c);
 *
 *  Page 1 is now assocated with Frame 28 (if we set the valid bit)
 */
void set_frame_number(p_entry_t * table_entry, p_number_t frame_number);


/*
 *  Print the page table in the following format
 *   --------------------------------------
 *  | page_number | V/I bit | frame_number |
 *   --------------------------------------
 *  Followed by the current number of valid entries and depiction of the
 *  current LRU queue (see linked_list.h for format details)
 *
 *  Example:
 *      print_page_table(table);
 */
void print_page_table (page_table * table);


/*
 *  Insert a page_number at bits 7-14
 */
void pt_set_page_number(p_entry_t * table_entry, p_number_t page_number);


/*
 *  Get the page number from bits 7-14
 */
p_number_t pt_get_page_number(p_entry_t table_entry);


void pt_print_entry(p_entry_t table_entry);

#endif
