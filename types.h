#include <stdint.h>

/*
 *https://pubs.opengroup.org/onlinepubs/009695399/basedefs/stdint.h.html
 */


/*
 *  16 bit entry of page table
 */
#define p_entry_t uint16_t


/*
 *  page and frame number are 8 bit fields
 */
#define p_number_t uint8_t


/*
 *  offset portion of logical/physical address is an 8 bit field
 */
#define addr_offset_t uint8_t


/*
 *  addresses to be read are 32 bit integers
 */
#define addr_t uint32_t


/*
 *  keys in the linked list are page or frame numbers
 */
#define ll_key_t p_number_t

/*
 *  values in the linked list are page table entries (for this project)
 */
#define ll_value_t p_entry_t

/*
 *  Because the hash table internally uses linked lists for its buckets,
 *  the key and value types which it is capable of storing are defined
 *  by the key and value types of the linked list.
 */
#define ht_key_t ll_key_t
#define ht_value_t ll_value_t


