#include "tlb.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>




tlb * init_tlb(int buckets)
{
    tlb * t = (tlb *)malloc(sizeof(tlb));
    hash_table * tlb = create_hash_table(buckets, 0);
 
    t->TLB = tlb;
    t->num_entries = 0;
    t->bucket_count = buckets;

    return t;
}


p_entry_t tlb_get(p_number_t key, tlb * t)
{
    p_entry_t page_table_entry = ht_get(key, t->TLB);

    return page_table_entry;
}


void tlb_insert(p_number_t key, p_entry_t value, tlb * t)
{
    if (t->num_entries == 16)
    {
        int index = 0;
        int min = INT_MAX;
        linked_list * list;
        
        for (int i = 0; i < t->bucket_count; i ++)
        {
            list = t->TLB->table[i];
            if (list->node_count > 0 && list->node_count < min)
            {
                min = list->node_count;
                index = i;
            }
        }


        ll_delete_back(t->TLB->table[index]);
        t->num_entries--;
    }

    ht_insert(key, value, t->TLB);
    t->num_entries++;
}


void tlb_delete(p_number_t key, tlb * t)
{
    p_entry_t entry = ht_get(key, t->TLB);

    ht_delete(key, t->TLB);

    if (t->num_entries > 0 && get_vi_bit(entry) == p_VALID)
        t->num_entries--;
}


void tlb_print(tlb * t)
{
    ht_print_table(t->TLB);
    printf("num entries: %d\n", t->num_entries);
}
