#include <stdio.h>
#include <stdlib.h>

#include "page_table.h"
#include "backing_store.h"
#include "addressing.h"
#include "tlb.h"


int main()
{
    page_table * ptable = init_page_table();
    tlb * TLB = init_tlb(48);
    char * main_mem = malloc(bs_page_size * 128);
    char buf[30] = {};
    addr_t addr = 0;

    //statistics
    int tlb_hits = 0;
    int num_addr = 0;
    int num_page_faults = 0;


    FILE * file = fopen("addresses.txt", "r");
    if (file == NULL)
    {
        printf("Cannot find addresses file\n");
        return -1;
    }

    while (fscanf(file, "%[^\n]%*c", buf) != EOF)
    {
        num_addr++;

        //read the logical addresses
        int r = sscanf(buf, "%d", &addr);
        if (r != 1)
        {
            printf("parsing err\n");
            return -2;
        }

        //Obtain the page number for this address
        p_number_t page_number = get_page_number(addr);


        //check if the page number is in the TLB
        p_entry_t tlb_result = tlb_get(page_number, TLB);
        if (get_vi_bit(tlb_result) == p_VALID)
        {
            //TLB HIT
            tlb_hits++;

            //load data directly and continue
            p_number_t frame_number = get_frame_number(tlb_result);
            addr_offset_t offset = get_offset(addr);

            //generate physical memory addr
            addr_t physical_addr = gen_phys_addr(frame_number, offset);

            //Update the LRU in the page table
            pt_insert(ptable, page_number, frame_number);


            char data = main_mem[physical_addr];
            printf("Virtual address: %d Physical address: %d Value: %d\n",
                addr, physical_addr, data);

            continue;
        }



        //TLB MISS
        p_entry_t table_entry = pt_search(ptable, page_number);
        p_number_t frame_number = get_frame_number(table_entry);

        //check the page number in page table
        if (get_vi_bit(table_entry) == p_INVALID)
        {
            num_page_faults++;

            //Handle page fault -- load into memory
            //generate phys addr with offset 0 to get location in mem
            addr_t load_point = gen_phys_addr(frame_number, 0);

            //load the page from backing store to destination (load_point)
            backstore_read(main_mem + load_point, page_number);


            p_number_t removed_page = pt_get_page_number(table_entry);
            tlb_delete(removed_page, TLB);
            set_valid(&table_entry);
        }

        //to generate loc to read in mem, we need frame num (in page table)
        //and offset
        addr_offset_t offset = get_offset(addr);

        //generate physical memory addr
        addr_t physical_addr = gen_phys_addr(frame_number, offset);

        //Update the LRU page table
        pt_insert(ptable, page_number, frame_number);

        //After each page table access, the TLB is updated
        tlb_insert(page_number, table_entry, TLB);


        char data = main_mem[physical_addr];
        printf("Virtual address: %d Physical address: %d Value: %d\n",
            addr, physical_addr, data);

    }


    printf("Number of Translated Addresses = %d\n\
Page Faults = %d\n\
Page Fault Rate = %1.3f\n\
TLB Hits = %d\n\
TLB Hit Rate = %1.3f\n",
num_addr, num_page_faults,
(float)num_page_faults / (float)num_addr,
tlb_hits,
(float)tlb_hits / (float)num_addr
    );


    return 0;
}
