#include <stdio.h>
#include <stdlib.h>

#include "page_table.h"

page_table * init_page_table ()
{
    page_table * table = (page_table *)malloc(sizeof(page_table));

    table->frame_count = 0;

    for (int i = 0; i < p_table_size; i++)
        table->p_table[i] = 0;

    table->p_table_queue = create_linked_list(p_max_entries);

    return table;
}


p_entry_t pt_search(page_table * table, p_number_t page_number)
{
    p_entry_t table_entry = table->p_table[page_number];
 
    if (pt_entry_valid(table_entry))
    {
        p_number_t frame_number = get_frame_number(table_entry);
        LRU_insert(page_number, frame_number, table->p_table_queue);
    }
    else if (pt_full(table))
    {
        table_entry = get_LRU_entry(table);
    }
    else
    {
        set_frame_number(&table_entry, table->frame_count);
        set_invalid(&table_entry);
    }
    
    
    return table_entry;
}


void pt_insert(page_table * table, p_number_t page_number, p_number_t frame)
{
    p_entry_t * table_entry = &(table->p_table[page_number]);

    if (table->frame_count < 128 && get_vi_bit(*table_entry) == p_INVALID)
        table->frame_count++;

    set_frame_number(table_entry, frame);
    set_valid(table_entry);
    pt_set_page_number(table_entry, page_number);

    LRU_insert(page_number, frame, table->p_table_queue);
}


p_entry_t get_LRU_entry(page_table * table)
{
    p_number_t frame_number = ll_get_LRU_value(table->p_table_queue);
    p_number_t page_number = ll_get_LRU_key(table->p_table_queue);

    ll_delete_back(table->p_table_queue);
    set_invalid(&(table->p_table[page_number]));

    table->frame_count--;

    p_entry_t removed_entry;
    pt_set_page_number(&removed_entry, page_number);
    set_frame_number(&removed_entry, frame_number);
    set_invalid(&removed_entry);

    return removed_entry;
}

int pt_entry_valid (p_entry_t table_entry)
{
  return get_vi_bit(table_entry);
}

int pt_full (page_table * table)
{
  return table->frame_count >= 128;
}

void set_valid(p_entry_t * table_entry)
{
    *table_entry |= p_valid_flag;
}

void set_invalid(p_entry_t * table_entry)
{
  *table_entry &= p_invalid_flag;
}

p_entry_t get_vi_bit(p_entry_t table_entry)
{
    return (table_entry & p_valid_flag) >> 0xf;
}

p_number_t get_frame_number(p_entry_t table_entry)
{
    return table_entry & p_frame_num_flag;
}

void pt_set_page_number(p_entry_t * table_entry, p_number_t page_number)
{
    p_entry_t bitmask = page_number;
    bitmask <<= p_frame_field_width;
    bitmask &= p_page_field_mask;
    *table_entry |= bitmask;
}


p_number_t pt_get_page_number(p_entry_t table_entry)
{
    return (table_entry & p_page_field_mask) >> p_frame_field_width;
}


void set_frame_number(p_entry_t * table_entry, p_number_t frame_number)
{
    frame_number &= p_frame_num_flag;
    *table_entry &= p_inverted_frame_field_mask;
    *table_entry |= frame_number;
}

void print_page_table (page_table * table)
{
    for (int i = 0; i < 256; i++)
    {
        printf("| %3d ", i);
        pt_print_entry(table->p_table[i]);
    }
    printf("Current Number of Valid Entries: %d\n", table->frame_count);

    print_linked_list(table->p_table_queue);
}

void pt_print_entry(p_entry_t table_entry)
{
    if (get_vi_bit(table_entry))
        printf("| V | ");
    else
        printf("| I | ");
    printf("%3d |", pt_get_page_number(table_entry));
    printf("%3d |\n", get_frame_number(table_entry));
}


