#include "page_table.h"
#include "types.h"

#define bs_page_size 0x100
#define bs_page_size_bits 8
#define bs_fname "BACKING_STORE.bin"

/*
 *  PREAMBLE
 *
 *  The backing_store.bin file is required to be in the project directory
 */


/*
 *  Reads the the specified page number from the backing store into the
 *  specified buffer.
 *
 *  Returns 0 on success, -1 on failure.
 */
int backstore_read(void * buffer, p_number_t page_number);
