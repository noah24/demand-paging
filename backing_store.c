#include <unistd.h>
#include <fcntl.h>
#include "backing_store.h"


/*
 *  Each of the functions within returns -1 on error, but some arbitrary
 *  value gte 0 on success. Each return must be checked for failure
 *  individually as a result.
 */
int backstore_read(void * buffer, p_number_t page_number)
{
    int fd = open(bs_fname, O_RDONLY);
    if (fd < 0)
        return -1;

    int r = lseek(fd, page_number << bs_page_size_bits, SEEK_SET);
    if (r < 0)
        return -1;

    r = read(fd, buffer, bs_page_size);
    if (r < 0)
        return -1;

    r = close(fd);
    if (r < 0)
        return -1;

    return 0;
}
