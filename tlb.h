#include "hash_table.h"
#include "page_table.h"
#include "types.h"

#define tlb_num_entries 16
#define tlb_cache_miss 0


/*
 *  TLB contains <page_number: page_table_entry> entries
 *  Implemented using a hash table
 */


typedef struct tlb
{
    hash_table * TLB;
    int num_entries;
    int bucket_count;
} tlb;


tlb * init_tlb(int buckets);
p_entry_t tlb_get(p_number_t key, tlb * t);
void tlb_insert(p_number_t key, p_entry_t value, tlb * t);
void tlb_delete(p_number_t key, tlb * t);
void tlb_print(tlb * t);
