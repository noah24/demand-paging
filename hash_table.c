#include <stdlib.h>
#include <stdio.h>

#include "hash_table.h"


hash_table * create_hash_table(unsigned int table_size,
                               unsigned int chain_length)
{
    hash_table * table = (hash_table *)malloc(
        ht_header_size + (table_size * sizeof(linked_list *))
    );

    table->table_size = table_size;


    for (int i = 0; i < table_size; i ++)
        table->table[i] = create_linked_list(chain_length);

    return table;
}


ht_value_t ht_get(ht_key_t key, hash_table *table)
{
    linked_list * list = ht_get_chain(key, table);
    ht_value_t value = ll_search(key, list);

    return value;
}


void ht_insert(ht_key_t key, ht_value_t value, hash_table * table)
{
    linked_list * list = ht_get_chain(key, table);
    LRU_insert(key, value, list);
}


void ht_delete(ht_key_t key, hash_table * table)
{
    linked_list * list = ht_get_chain(key, table);
    ll_delete(key, list);
}


linked_list * ht_get_chain(ht_key_t key, hash_table * table)
{
    return table->table[compress(n_hash(key), table)];
}

/*
 *  Testing function
 */
void ht_print_table(hash_table * table)
{
    printf("{\n\n");
    for (int i = 0; i < table->table_size; i ++)
    {
        printf("    ");
        print_linked_list( table->table[i] );
    }
    printf("}\n");
}


ht_key_t rotl8(ht_key_t byte, unsigned int shift)
{
    shift %= 8;
    return (byte << shift) | (byte >> (8 - shift));
}


ht_key_t rotr8(ht_key_t byte, unsigned int shift)
{
    shift %= 8;
    return (byte >> shift) | (byte << (8 - shift));
}


int n_hash(ht_key_t key)
{
    int h = 0;
    for (int i = 0; i < 8; i ++)
        h ^= rotl8(h, 5) | rotr8(h, 3) + rotl8(key, i);

    return h;
}


ht_key_t compress(int hash, hash_table * table)
{
    return (hash >= 0) ? hash % table->table_size
        : -1 * hash % table->table_size;
}
